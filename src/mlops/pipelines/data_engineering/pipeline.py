
from kedro.pipeline import Pipeline, node
from .nodes import preprocess_images, train_yolov5
def preprocess_train_images_with_fraction(train_images, train_labels):
    return preprocess_images(train_images, train_labels, fraction=0.1)
def preprocess_valid_images_with_fraction(valid_images, valid_labels):
    return preprocess_images(valid_images, valid_labels, fraction=1.0)
def create_pipeline(**kwargs):
    return Pipeline(
        [
            
            node(
                func=train_yolov5,
                inputs= None, #["processed_train_images", "processed_train_labels", "processed_valid_images", "processed_valid_labels"],
                outputs="yolov5_model",
                name="train_yolov5_model"
            ),
        ]
    )
    
"""node(
                func=preprocess_train_images_with_fraction,
                inputs=["train_images", "train_labels"],
                outputs=["processed_train_images", "processed_train_labels"],
                name="preprocess_train_data"
            ),
            node(
                func=preprocess_valid_images_with_fraction,
                inputs=["valid_images", "valid_labels"],
                outputs=["processed_valid_images", "processed_valid_labels"],
                name="preprocess_valid_data"
        ),"""
