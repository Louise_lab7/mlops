import os
import numpy as np
from PIL import Image
from pathlib import Path
import yaml
from yolov5 import train


def preprocess_images(images, labels, fraction=1.0):
    # Sélectionner aléatoirement une fraction des images et des labels
    total_images = len(images)
    indices = np.random.choice(total_images, int(total_images * fraction), replace=False)
    processed_images = [images[i] for i in indices]
    processed_labels = [labels[i] for i in indices]
    
    return processed_images, processed_labels
def train_yolov5(): #train_images, train_labels, valid_images, valid_labels
    
    print(os.getcwd())
    results = train.run(data='data/data.yaml', imgsz=640, epochs=1, batch=8, weights='yolov5s.pt')
    return results