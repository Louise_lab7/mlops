from mlops.pipelines.data_engineering import pipeline as de_pipeline

def register_pipelines():
    return {
        "de": de_pipeline.create_pipeline(),
        "__default__": de_pipeline.create_pipeline(),
    }